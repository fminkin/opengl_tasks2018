#include <iostream>
#include <cstring>
#include <cstdlib>
#include "maze.h"

int main(int argc, char** argv) {
    std::unique_ptr<MazeApplication> app_holder;

    if (argc < 2) {
        std::cerr << "You must specify type of maze generation. Possible options: [generate, load]\n";
        exit(1);
    }
    if (strcmp(argv[1], "generate") == 0) {
        std::cerr << "Generating is not implemented yet.\n";
        exit(1);
    } else if (strcmp(argv[1], "load") == 0) {
        if (argc < 3) {
            std::cerr << "For loading the maze you must specify the filename inside data folder\n";
            exit(1);
        }
        float cell_size = 0.5;
        if (argc == 4) {
            cell_size = std::strtod(argv[4], 0);
        }
        std::cout << "Loading predefined maze with cell size of " << cell_size << "\n";
        app_holder.reset(new MazeApplication(argv[2], cell_size));
    } else {
        std::cerr << "Type of maze generation " << argv[1] << " is not supported\n";
        exit(1);
    }
    std::cout << "Launching app\n";


    app_holder->start();
    return 0;
}