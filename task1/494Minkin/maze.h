//
// Created by Fedor Minkin on 09.03.18.
//

#ifndef STUDENTTASKS2017_MAZE_H
#define STUDENTTASKS2017_MAZE_H

#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <iostream>

#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "Camera.hpp"

enum CameraPosition {
  FACE = 0,
  ORBIT
};

enum WallPosition {
  BOTTOM = 0,
  TOP,
  WEST,
  EAST,
  SOUTH,
  NORTH
};

enum State {
  FREE = 0,
  UNAVAILABLE
};

class MazeApplication : public Application {
public:
  MazeApplication(const std::string &maze_config_filename, const float cell_size);

  MazeApplication(const float cell_size);

  void makeScene() override;
  void draw() override;
  void handleKey(int key, int scancode, int action, int mods) override;

private:
  std::vector<std::vector<State>> maze_holder_;

  float cell_size_;
  int camera_position_;
  int W_, H_;

  std::vector<MeshPtr> meshes_;
  std::vector<glm::mat4> cellsTranslations;

  ShaderProgramPtr shader;

  std::vector<WallPosition> CalculateSurroundings(size_t i, size_t j);

  void FillCellVertices(size_t i, size_t j, std::vector<glm::vec3> & cellVertices);
  void AddToTriangleCoordinates(WallPosition position,
                                std::vector<glm::vec3> *triangle_coordinates,
                                const std::vector<glm::vec3> &cellVertices);
  void AddNormals(WallPosition position, std::vector<glm::vec3> *normals_coordinates);
  void AddCoordinatesToTextureCoordinates(std::vector<glm::vec2> *texture_coordinates);
  MeshPtr MakeMeshCell(size_t i, size_t j, std::vector<WallPosition> surroundings);
};

#endif //STUDENTTASKS2017_MAZE_H
