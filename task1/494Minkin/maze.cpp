#include "maze.h"
#include <algorithm>
#include <map>

static const std::map<WallPosition, glm::vec3> kNormals = {
  {
    WallPosition::WEST,   glm::vec3(1.0, 0.0, 0.0)
  },
  {
    WallPosition::EAST,   glm::vec3(-1.0, 0.0, 0.0)
  },
  {
    WallPosition::NORTH,  glm::vec3(0.0, 1.0, 0.0)
  },
  {
    WallPosition::SOUTH,  glm::vec3(0.0, -1.0, 0.0)
  },
  {
    WallPosition::BOTTOM, glm::vec3(0.0, 0.0, 1.0)
  },
  {
    WallPosition::TOP,    glm::vec3(0.0, 0.0, -1.0)
  }
};

static const std::map<WallPosition, std::vector<size_t>> kWallCoordinates = {
  {
    WallPosition::WEST,   std::vector<size_t> {0, 4, 7, 7, 3, 0}
  },
  {
    WallPosition::EAST,   std::vector<size_t> {1, 5, 6, 6, 2, 1}
  },
  {
    WallPosition::NORTH,  std::vector<size_t> {0, 4, 5, 5, 1, 0}
  },
  {
    WallPosition::SOUTH,  std::vector<size_t> {3, 7, 6, 6, 2, 3}
  },
  {
    WallPosition::BOTTOM, std::vector<size_t> {0, 1, 2, 2, 3, 0}
  },
  {
    WallPosition::TOP,    std::vector<size_t> {4, 5, 6, 6, 7, 4}
  }
};

static const std::vector<glm::vec2> kTextureOrder {
  glm::vec2(0.0, 0.0),
  glm::vec2(1.0, 0.0),
  glm::vec2(1.0, 1.0),
  glm::vec2(1.0, 1.0),
  glm::vec2(0.0, 1.0),
  glm::vec2(0.0, 0.0)
};

static const std::vector<std::pair<int, int>> kSignOrder {
  std::make_pair(-1, -1),
  std::make_pair(-1, 1),
  std::make_pair(1, 1),
  std::make_pair(1, -1),
};

State convert_to_state(char c) {
  return c == ' ' ? State::FREE : State::UNAVAILABLE;
}

std::vector<State> convert_to_vector(const std::string &maze_st) {
  std::vector<State> ret;
  for (const char& c: maze_st) {
    ret.push_back(convert_to_state(c));
  }
  return ret;
}

MazeApplication::MazeApplication(const std::string &maze_config_filename, const float cell_size) : cell_size_(cell_size) {
  camera_position_ = CameraPosition::FACE;
  std::ifstream input_stream(maze_config_filename);

  std::string maze_str;
  maze_holder_.clear();
  while (std::getline(input_stream, maze_str)) {
    maze_holder_.push_back(convert_to_vector(maze_str));
  }
  H_ = maze_holder_.size();
  assert(maze_holder_.size() > 0);
  W_ = maze_holder_[0].size();
}

MazeApplication::MazeApplication(const float cell_size) {
  // generation not implemented
}

std::vector<WallPosition> MazeApplication::CalculateSurroundings(size_t i, size_t j) {
  if (maze_holder_[i][j] == State::UNAVAILABLE) {
    return std::vector<WallPosition> {WEST, EAST, TOP, BOTTOM, NORTH, SOUTH};
  }

  std::vector<WallPosition> surroundings {TOP, BOTTOM};
  return surroundings;
}


void MazeApplication::makeScene() {
  Application::makeScene();

  for (size_t i = 0; i < H_; ++i) {
    for (size_t j = 0; j < W_; ++j) {
      std::vector<WallPosition> surroundings = CalculateSurroundings(i, j);
      meshes_.push_back(MakeMeshCell(i, j, surroundings));
      cellsTranslations.emplace_back(glm::mat4(1.0f));
    }
  }

  if (camera_position_ == CameraPosition::FACE) {
    _cameraMover = std::make_shared<FreeCameraMover>();
  } else {
    _cameraMover = std::make_shared<OrbitCameraMover>();
  }

  shader = std::make_shared<ShaderProgram>("494MinkinData/shaders/shaderNormal.vert", "494MinkinData/shaders/shader.frag");
}


void MazeApplication::draw() {
  Application::draw();

  int _width, _height;
  glfwGetFramebufferSize(_window, &_width, &_height);

  glViewport(0, 0, _width, _height);

  //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //Подключаем шейдер
  shader->use();

  //Устанавливаем общие юниформ-переменные
  shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
  shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

  //Рисуем
  for (int i = 0; i < H_ * W_; ++i) {
    shader->setMat4Uniform("modelMatrix", cellsTranslations[i]);
    meshes_[i]->draw();
  }
}


void MazeApplication::handleKey(int key, int scancode, int action, int mods)
{
  if (action == GLFW_PRESS)
  {
    if (key == GLFW_KEY_ESCAPE)
    {
      glfwSetWindowShouldClose(_window, GL_TRUE);
    }
    if (key == GLFW_KEY_F) {
      _cameraMover = std::make_shared<FreeCameraMover>();
    }
    if (key == GLFW_KEY_O) {
      _cameraMover = std::make_shared<OrbitCameraMover>();
    }
  }
  _cameraMover->handleKey(_window, key, scancode, action, mods);
}


void MazeApplication::FillCellVertices(size_t i, size_t j, std::vector<glm::vec3> & cell_vertices) {
  float size = cell_size_ / 2;
  float horizontal = cell_size_ * i + size;
  float vertical = cell_size_ * j + size;

  for (int cell_size_multiplier = 0; cell_size_multiplier <= 1; ++cell_size_multiplier) {
    for (const auto &sign_pair: kSignOrder) {
      cell_vertices.emplace_back(glm::vec3(horizontal + sign_pair.first * size,
                                          vertical + sign_pair.second * size,
                                          cell_size_multiplier * cell_size_));
    }
  }
}


void MazeApplication::AddCoordinatesToTextureCoordinates(std::vector<glm::vec2> *texture_coordinates) {
  texture_coordinates->insert(std::begin(*texture_coordinates), std::begin(kTextureOrder), std::end(kTextureOrder));

}

void MazeApplication::AddToTriangleCoordinates(WallPosition position, std::vector<glm::vec3> *triangle_coordinates,
                                               const std::vector<glm::vec3> &cellVertices) {
  for (const auto &cube_coordinate_index: kWallCoordinates.at(position)) {
    triangle_coordinates->push_back(cellVertices[cube_coordinate_index]);
  }
}


void MazeApplication::AddNormals(WallPosition position, std::vector<glm::vec3> *normals_coordinates) {
  std::fill_n(std::back_inserter(*normals_coordinates), kNormals.size(), kNormals.at(position));
}


MeshPtr MazeApplication::MakeMeshCell(size_t i, size_t j, std::vector<WallPosition> surroundings) {
  std::vector<glm::vec3> triangle_coordinates;
  std::vector<glm::vec3> cell_vertices;
  std::vector<glm::vec3> normals;
  std::vector<glm::vec2> texture_coordinates;

  FillCellVertices(i, j, cell_vertices);

  for (const auto &type: surroundings) {
    AddToTriangleCoordinates(type, &triangle_coordinates, cell_vertices);
    AddNormals(type, &normals);
    AddCoordinatesToTextureCoordinates(&texture_coordinates);
  }

  DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf0->setData(triangle_coordinates.size() * sizeof(float) * 3, triangle_coordinates.data());

  DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

  DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf2->setData(texture_coordinates.size() * sizeof(float) * 2, texture_coordinates.data());

  MeshPtr mesh = std::make_shared<Mesh>();
  mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
  mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
  mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
  mesh->setPrimitiveType(GL_TRIANGLES);
  mesh->setVertexCount(triangle_coordinates.size());

  return mesh;
}
